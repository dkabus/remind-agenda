APP=remind-agenda
PREFIX=/usr/local
MANPREFIX=$(PREFIX)/share/man

all:

install: $(APP)
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f $(APP) $(DESTDIR)$(PREFIX)/bin/$(APP)
	chmod 755 $(DESTDIR)$(PREFIX)/bin/$(APP)
	# mkdir -p $(DESTDIR)$(MANPREFIX)/man1
	# sed "s/VERSION/$(VERSION)/g" < $(APP).1 > $(DESTDIR)$(MANPREFIX)/man1/$(APP).1
	# chmod 644 $(DESTDIR)$(MANPREFIX)/man1/$(APP).1
	mkdir -p $(DESTDIR)$(PREFIX)/share/applications
	cp -f $(APP).desktop $(DESTDIR)$(PREFIX)/share/applications

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/$(APP)
	# rm -f $(DESTDIR)$(MANPREFIX)/man1/$(APP).1
	rm -f $(DESTDIR)$(PREFIX)/share/applications/$(APP).desktop

.PHONY: all install uninstall
