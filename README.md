# Remind Agenda TUI

This is a simple interactive terminal interface for the calendar program
['remind'](https://dianne.skoll.ca/projects/remind/).

![Screenshot of Remind Agenda](screenshot.png)

## Usage

All arguments are passed on to `remind`, except for the `-I` flag, which toggles interactive mode:

```sh
remind-agenda -I -@b1mg
```

## Installation

Add this script to your PATH, for instance via `make install`.

For Arch Linux, there is also the
[AUR package `remind-agenda-git`](https://aur.archlinux.org/packages/remind-agenda-git).
